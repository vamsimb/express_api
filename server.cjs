const express = require('express');
const apiRoute = require('./src/student/routes.cjs')
const app = express();
const { port } = require('./src/student/config')

app.use(express.json());

app.get('/', (req, res) => {
   res.send('Hey world!');
})

app.use('/api', apiRoute);

app.listen(port, () => {
   console.log("okok listening on" + port)
})