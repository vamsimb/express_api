const { Pool } = require("pg");

const { user, host, database, password } = require("./src/student/config")

const pool = new Pool({
   user,
   host,
   database,
   password
})

module.exports = pool;