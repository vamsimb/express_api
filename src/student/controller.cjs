const pool = require('../../database.cjs');
const queries = require('./queries.cjs');

const getStudents = (req, res) => {
   pool.query(queries.getStudents, (error, data) => {
      if (error) {
         throw error
      } else {
         res.status(200).json(data.rows)
      }
   })
}

const getStudentById = (req, res) => {
   const id = parseInt(req.params.id)
   pool.query(queries.getStudentById, [id], (error, data) => {
      if (error) {
         throw error
      } else {
         res.status(200).json(data.rows)
      }
   })
}

const addStudent = (req, res) => {
   const { name, email, age, dob } = req.body;
   pool.query(queries.checkEmailExists, [email], (error, data) => {
      if (error) {
         throw error
      } else {
         if (data.rows.length) {
            res.send("email already exists")
         } else {
            pool.query(queries.addStudent, [name, email, age, dob], (error, data) => {
               if (error) {
                  throw error
               } else {
                  res.status(201).send("Student created successfully")
               }
            })
         }
      }
   })
}



const removeStudent = (req, res) => {

   const id = parseInt(req.params.id);
   pool.query(queries.getStudentById, [id], (error, results) => {

      if (error) {
         throw error;
      } else {
         const noData = !results.rows.length;

         if (noData) {
            res.send("Student does not exist in the database");
         } else {
            pool.query(queries.removeStudent, [id], (error, results) => {
               if (error) {
                  throw error
               } else {
                  res.status(200).send("Student deleted from the database");
               }
            })
         }
      }
   })
}

const updateStudent = (req, res) => {
   const id = parseInt(req.params.id);
   const { name, email, age, dob } = req.body;
   pool.query(queries.getStudentById, [id], (error, results) => {

      if (error) {
         throw error;
      } else {
         const noStudentfound = !results.rows.length;
         if (noStudentfound) {
            res.send("Student does not exist in the database");
         } else {

            pool.query(queries.updateStudent, [
               name,
               id,
            ],
               (error, results) => {
                  if (error) {
                     throw error;
                  } else {
                     res.status(200).send("Student updated in the database");
                  }
               }
            );
         }
      }
   });
};


module.exports = {
   getStudents,
   getStudentById,
   addStudent,
   removeStudent,
   updateStudent
}
