const getStudents = "SELECT * FROM API_TABLE";
const getStudentById = "SELECT * FROM API_TABLE WHERE id = $1";
const checkEmailExists = "SELECT s FROM API_TABLE s WHERE s.email = $1";
const addStudent = "INSERT INTO API_TABLE (name, email, age, dob) VALUES ($1, $2, $3, $4)";

const removeStudent = "DELETE FROM API_TABLE WHERE id = $1";

const updateStudent = "UPDATE API_TABLE SET name = $1 WHERE id =$2";


module.exports = {
   getStudents,
   getStudentById,
   checkEmailExists,
   addStudent,
   removeStudent,
   updateStudent
}