const dotenv = require('dotenv');

dotenv.config()

module.exports = {
   port: process.env.PORT,
   user: process.env.USER_NAME,
   host: process.env.HOST,
   password: process.env.PASSWORD,
   database: process.env.DATABASE
}